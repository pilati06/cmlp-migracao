<!DOCTYPE html>
<html>

    <head>

        <?php wp_head(); ?>
    </head>

<body <?php body_class(); ?>>

<header>
    <div class="pt-2 pb-2">
        <div class="container">
            <div class="row top-menu-row">
                <div class="col-lg-5">
                    <div class="row">
                        <div class="col-6">
                            <a href="http://www.uel.br/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/uel.png" width=100%></a>
                        </div>
                        <div class="col-6">
                            <a href="http://www.uel.br/proppg/portalnovo/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/proppg.jpg" width=100%></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <?php wp_nav_menu(
                        array(
                            'theme_location' => 'top-menu',
                            'menu_class' => 'd-flex flex-row'
                        )
                    ); ?>    
                </div>
            </div>
        </div>
    </div>

    <div class="main-menu-row">
        <div class="container">
            <div class="row">
                    <div class="col-lg-2 pt-2 pb-2">
                        <?php echo get_custom_logo(); ?>
                    </div>
                    <div class="col-lg-10 d-none d-lg-block">
                        <?php wp_nav_menu(
                            array(
                                'theme_location' => 'menu-statistics',
                                'menu_class' => 'menu-statistics d-flex flex-row float-right'
                            )
                        ); ?>
                        <?php wp_nav_menu(
                            array(
                                'theme_location' => 'main-menu',
                                'menu_class' => 'main-menu d-flex flex-row float-right'
                            )
                        ); ?>
                    </div>
            </div>
        </div>
    </div>
    <div class="main-menu-row fixed d-none d-lg-block">
        <div class="container">
            <div class="row">
                    <div class="col-lg-2 pt-2 pb-2">
                        <?php echo get_custom_logo(); ?>
                    </div>
                    <div class="col-lg-10">
                        <?php wp_nav_menu(
                            array(
                                'theme_location' => 'menu-statistics',
                                'menu_class' => 'menu-statistics d-flex flex-row float-right'
                            )
                        ); ?>
                        <?php wp_nav_menu(
                            array(
                                'theme_location' => 'main-menu',
                                'menu_class' => 'main-menu d-flex flex-row float-right'
                            )
                        ); ?>
                    </div>
            </div>
        </div>
    </div>    
</header>

<div class="container">