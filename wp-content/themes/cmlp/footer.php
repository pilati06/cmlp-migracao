</div> <!-- container -->

<div class="container">
    <div class="row">
            
        <div class="col-lg-12">
            <h3 class="footer-slider-title">Fontes de Fomento</h3>
            <?php echo do_shortcode('[slide-anything id="132"]'); ?>
        </div>

    </div>

    <div class="row">

        <div class="col-lg-3 col-md-4">
            <div class="row col-12">
                <h3 class="footer-title-contact">Onde estamos?</h3>
            </div>

            <ul class="address-footer">
                <li><i class="fas fa-map-marker-alt"></i> Rodovia Celso Garcia Cid</br>
                PR 445 Km 380</br>
                Campus Universitário</br>
                Londrina – PR, 86057-970</li>

                <li><i class="fas fa-phone-alt"></i> +55 43 3371 4503</li>

                <li><a href="mailto:cmlp@uel.br"><i class="fas fa-envelope"></i> cmlp@uel.br</a></li>

                <li><a href="#"><i class="fab fa-facebook-square"></i> facebook</a></li>

                <li><a href="#"><i class="fab fa-linkedin"></i> linkedin</a></li>

                <li><a href="#"><i class="fab fa-instagram"></i> Instagram</a></li>

                <li><a href="#"><i class="fab fa-twitter-square"></i> twitter</a></li>
        </ul>
        </div>
        <div class="col-lg-4 col-md-8">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14655.07019851506!2d-51.2003547!3d-23.3241912!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8cd1fb9730cff6b!2sUniversidade%20Estadual%20de%20Londrina!5e0!3m2!1spt-BR!2sbr!4v1602288276511!5m2!1spt-BR!2sbr" width="100%" height="350" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
        
        <div class="col-lg-5 footer-contact-form">
            <div class="row col-12">
                <h3 class="footer-title-contact">Ficou com alguma dúvida?</h3>
            </div>
            <?php echo do_shortcode('[contact-form-7 id="228" title="Contact form 1"]'); ?>
        </div>

    </div>
</div>

<footer>
    <div class="container">

        <div class="row">

                <div class="col-md-3 col-sm-6">
                    <h6>A Universidade</h6>
                    <?php wp_nav_menu(
                        array(
                            'theme_location' => 'universidade',
                        )
                    );?>
                </div>
                
                <div class="col-md-3 col-sm-6">
                    <h6>Ensino</h6>
                    <?php wp_nav_menu(
                        array(
                            'theme_location' => 'ensino',
                        )
                    );?>
                </div>

                <div class="col-md-3 col-sm-6">
                    <h6>Cominidade</h6>
                    <?php wp_nav_menu(
                        array(
                            'theme_location' => 'comunidade',
                        )
                    );?>
                </div>

                <div class="col-md-3 col-sm-6">
                    <h6>Serviço da UEL</h6>
                    <?php wp_nav_menu(
                        array(
                            'theme_location' => 'servico',
                        )
                    );?>
                </div>


        </div>
        <div class="row">

            <div class="col-lg-12 text-center"><p class="copyright">&copy; <?php echo date('Y'); ?> CMLP - Todos os direitos reservados</p></div>
        
        </div>

        <?php wp_footer(); ?>
    </div>    
</footer>
<a href="#top" id="to-top"><i class="fas fa-arrow-up"></i></a>

</body>
</html>