<?php get_header(); ?>

<div class="content pt-5 pb-5">

    <h1 class="mb-4"><?php wp_title(''); ?></h1>

    <ul class="lab-list">
        <?php if (have_posts()) : while(have_posts()) : the_post(); ?>

            <li class="col-lg-12 border mb-4">
                <div class="row">
                    <?php if (has_post_thumbnail()): ?>
                        <div class="col-lg-3">
                            <div class="img-lab-list">
                                    
                                <div>
                                    <a href="<?php the_permalink(); ?>">
                                        <!-- <span class="popular-hover"><small>Saiba mais</small></span> -->
                                        <img width=100% src="<?php the_post_thumbnail_url('small');?>" class="img-fluid">
                                    </a>
                                </div>
                                        
                            </div>
                        </div>
                    <?php endif; ?>

                    <div class="<?php if (has_post_thumbnail()): ?>col-lg-9<?php else: ?>col-12<?php endif; ?>">
                        <div class="p-4">
                            <h3 class="news-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            <?php the_excerpt(); ?>

                            <?php if (get_field( 'link_para_laboratorio' )): ?>
                                <a class="lab-button" href="<?php the_field( 'link_para_laboratorio' ); ?>" target="_blank">Visite o site do laboratório</a>
                                <a class="lab-button d-block d-md-none" href="<?php the_permalink(); ?>" target="_blank">Saiba mais</a>
                            <?php endif; ?>
                        
                            <a class="news-button" href="<?php the_permalink(); ?>">Leia mais</a>
                        </div>
                    </div>

                </div>

            </li>
        <?php endwhile; endif; ?>
    </ul>

    <?php the_posts_pagination(array(
        'mid_size' => 2,
        'prev_text' => '<span class="prev-page">Anterior</span>',
        'next_text' => '<span class="next-page">Próximo</span>',
        'screen_reader_text' => ' ',
        'before_page_number' => '',
    )); ?>
    

</div>

<?php get_footer(); ?>