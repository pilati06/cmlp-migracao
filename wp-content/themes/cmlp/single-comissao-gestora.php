<?php get_header(); ?>

<div class="content pt-5">
    <h1 class="mb-4"><?php the_title(); ?></h1>
    <p class="subtitle mb-2"><?php the_field( 'funcao_do_gestor' ); ?></p>
    <p class="mb-4"><?php if (get_field( 'email' )): ?><a class="pt-3 pb-3" href="<?php echo 'mailto:'; the_field( 'email' ); ?>" ><i class="fas fa-envelope mr-2"></i><?php the_field( 'email' ); ?></a><?php endif; ?></p>
    <div class="border p-3 mr-3 mb-3 float-left text-center col-sm-12 col-md-6 col-lg-4">
        <?php if (has_post_thumbnail()): ?>
            <div><img src="<?php the_post_thumbnail_url('small');?>" class="img-fluid">
        <?php else: ?>
            <i style="width:300px; height:300px;font-size: 300px;" class="fas fa-user"></i>
        <?php endif; ?>
    </div>

    <?php if (have_posts()) : while(have_posts()) : the_post(); ?>
        <?php the_content(); ?>
    <?php endwhile; endif; ?>

    <ul class="info-gestor clearfix">
        <?php if (get_field( 'link_lattes' )): ?>
            <li class="mr-3 mt-3 float-left"><a class="p-3" target="_blank" href="<?php the_field( 'link_lattes' ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/lattes.png" width=75></a></li>
        <?php endif; ?>
        
        <?php if (get_field( 'link_researchgate' )): ?>
            <li class="mr-3 mt-3 float-left"><a class="p-3" target="_blank" href="<?php the_field( 'link_researchgate' ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/research_gate.png" width=75></a></li>
        <?php endif; ?>

        <?php if (get_field( 'link_orcid' )): ?>
            <li class="mr-3 mt-3 float-left"><a class="p-3" target="_blank" href="<?php the_field( 'link_orcid' ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/orcid.png" width=75></a></li>
        <?php endif; ?>
    </ul>
    
    <div class="clearfix"></div>
    
</div>

<?php get_footer(); ?>