<?php

function load_stylesheets() {

    wp_register_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), false, 'all');
    wp_enqueue_style('bootstrap');
    
    wp_register_style('fontawesome', 'https://pro.fontawesome.com/releases/v5.10.0/css/all.css', array(), false, 'all');
    wp_enqueue_style('fontawesome');

    wp_register_style('style', get_template_directory_uri() . '/style.css', array(), false, 'all');
    wp_enqueue_style('style');

}
add_action('wp_enqueue_scripts', 'load_stylesheets');

function include_jquery(){

    wp_deregister_script('jquery');
    wp_register_script('jquery', 'https://code.jquery.com/jquery-3.5.1.min.js', '', 1, true);
    wp_enqueue_script('jquery');

}
add_action('wp_enqueue_scripts', 'include_jquery');

function load_scripts() {

    wp_register_script('customjs', get_template_directory_uri() . '/js/scripts.js', '', 1, true);
    wp_enqueue_script('customjs');

}
add_action('wp_enqueue_scripts', 'load_scripts');

function custom_posts_per_page_gestao( $query ) {

    if ( $query->is_post_type_archive('comissao-gestora') ) {
        set_query_var('posts_per_page', -1);
    }
}
add_action( 'pre_get_posts', 'custom_posts_per_page_gestao' );

function rtcadmin_show_cpt_archives( $query ) {
    if ( ! empty( $query->query_vars['suppress_filters'] ) ) return;
    if ( ! is_category() && ! is_tag() ) return;
    if ( ! $query->is_main_query() ) return;

    $post_types = $query->get( 'post_type' );
    $post_types = ( empty( $post_types ) ) ? [ 'post' ] : $post_types;
    $post_types = array_merge( (array) $post_types, [ 'equipamentos' ] );

    $query->set( 'post_type', $post_types );
}

add_filter( 'pre_get_posts', 'rtcadmin_show_cpt_archives' );

add_theme_support('menus');
add_theme_support( 'custom-logo' );
add_theme_support('post-thumbnails');

register_nav_menus(
    array(
        'top-menu' => __('Top Menu', 'theme'),
        'main-menu' => __('Main Menu', 'theme'),
        'universidade' => __('Menu Footer Universidade', 'theme'),
        'ensino' => __('Menu Footer Ensino', 'theme'),
        'comunidade' => __('Menu Footer Comunidade', 'theme'),
        'servico' => __('Menu Footer Serviço', 'theme'),
        'menu-statistics' => __('Menu Estatísticas', 'theme'),
    )
);

add_image_size('small-150', 150, 150, true);
add_image_size('small', 300, 300, true);
add_image_size('medium', 600, 600, true);
add_image_size('large', 1110, 350, true);
