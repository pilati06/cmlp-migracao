<?php /*get_header(); ?>

<div class="content pt-5 pb-5">
    <h1><?php single_cat_title(); ?></h1>

    <?php if (have_posts())  : while(have_posts()) : the_post(); ?>
        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        <?php the_excerpt(); ?>
        <a href="<?php the_permalink(); ?>">Leia mais</a>
    <?php endwhile; endif; ?>

    <?php the_posts_pagination(array(
        'mid_size' => 2,
        'prev_text' => '<span class="prev-page">Anterior</span>',
        'next_text' => '<span class="next-page">Próximo</span>',
        'screen_reader_text' => ' ',
        'before_page_number' => '',
    )); ?>

</div>

<?php get_footer();*/ ?>
<?php get_header(); ?>

<div class="content pt-5 pb-5">
    
    <h1 class="mb-4"><?php wp_title(''); ?></h1>

    <ul class="equipament-list">
        <?php if (have_posts()) : while(have_posts()) : the_post(); ?>
            <li class="col-lg-12 border mb-4">
                <div id="modal_<?php the_id(); ?>" class="modal-img">
                    <a href="#close" class="modal-img-overlay"></a>
                    <span class="modal-img-content">
                        <!-- <a id="close-img-modal" href="#close">X</a> -->
                        <img class="img-elm-modal" src="<?php the_post_thumbnail_url('medium');?>" class="img-fluid">
                    </span>
                </div>
                <div class="row">
                    
                    <div class="col-lg-2 col-md-4">
                        <div class="img-equipament-list">
                            <?php if (has_post_thumbnail()): ?>
                                <a href="#modal_<?php the_id(); ?>">
                                    <div>
                                        <span class="popular-hover"><small><i class="fas fa-search-plus"></i></small></span>
                                        <img src="<?php the_post_thumbnail_url('small');?>" class="img-fluid" width=100%>
                                    </div>
                                </a>
                            <?php else: ?>
                                <img src="<?php echo get_template_directory_uri(); ?>/images/not-found.png" width=100%>
                            <?php endif; ?>
                        </div>
                        <div class="after-strip"></div>
                    </div>

                    <div class="col-lg-10 col-md-8">
                        <div class="p-4">
                            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            
                            <div class="mb-4 mt-3"><?php the_category(); ?></div>
                            
                            <?php the_excerpt(); ?>
                            
                            <a class="lab-button equipament-button float-right" href="<?php the_permalink(); ?>">Saiba mais</a>

                            <!-- <a href="<?php the_permalink(); ?>">Leia mais</a> -->
                        </div>
                        
                    </div>

                </div>
            </li>

        <?php endwhile; endif; ?>
    </ul>

    <?php the_posts_pagination(array(
        'mid_size' => 2,
        'prev_text' => '<span class="prev-page">Anterior</span>',
        'next_text' => '<span class="next-page">Próximo</span>',
        'screen_reader_text' => ' ',
        'before_page_number' => '',
    )); ?>

</div>

<?php get_footer(); ?>