$(document).ready(function(){
    // Quando efetuar o scroll ativa a funÃ§Ã£o toTop
    $(window).on('scroll', function () {
        toTop();
    });

    $(document).mouseup(function(e) 
    {
        var container = $("ul.main-menu > li > .sub-menu");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0) 
        {
            $('ul.main-menu > li').removeClass('active');
        }
    });

    $('#to-top').click(function(){
        $('html, body').animate({scrollTop:0}, 'slow');
        return false;
    });

    $('ul.main-menu > li').click(function(){
        $('ul.main-menu > li').removeClass('active');
        $(this).addClass('active');
    });

    function toTop() {
        try {

            if ($(window).scrollTop() > 220){
                $(".main-menu-row.fixed, #to-top").addClass("show");
            } else {
                $(".main-menu-row.fixed, #to-top").removeClass("show");
            }

        } catch(e) {
            console.log(e.message);
        }
    }
});