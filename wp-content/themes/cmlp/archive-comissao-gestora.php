<?php get_header(); ?>

<div class="content pt-5">

    <h1 class="mb-4"><?php wp_title(''); ?></h1>
    <p>A comissão gestora é formada por docentes de vários centros de estudo e de diferentes áreas do conhecimento.</p>

    <?php if (have_posts()) : while(have_posts()) : the_post(); ?>

        <div class="col-md-12 border p-3 mb-3 gestor">

            <div class="row">

                <div class="col-lg-2 col-md-3 text-center">
                    <?php if (has_post_thumbnail()): ?>
                        <img src="<?php the_post_thumbnail_url('small-150');?>" class="img-fluid">
                    <?php else: ?>
                        <i style="width:150px; height:150px;font-size: 150px;" class="fas fa-user"></i>
                    <?php endif; ?>
                </div>

                <div class="col-lg-8 col-md-6">
                    <!-- Código título com link -->
                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <p><?php the_field( 'funcao_do_gestor' ); ?></p>
                    <a class="p-3" href="<?php echo 'mailto:'; the_field( 'email' ); ?>" ><i class="fas fa-envelope mr-2"></i><?php the_field( 'email' ); ?></a>
                </div>

                <div class="col-lg-2 col-md-3">

                    <ul class="info-gestor">
                        <?php if (get_field( 'link_lattes' )): ?>
                            <li class="mb-3"><a class="p-3" target="_blank" href="<?php the_field( 'link_lattes' ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/lattes.png" width=75></a></li>
                        <?php endif; ?>
                        
                        <?php if (get_field( 'link_researchgate' )): ?>
                            <li class="mb-3"><a class="p-3" target="_blank" href="<?php the_field( 'link_researchgate' ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/research_gate.png" width=75></a></li>
                        <?php endif; ?>

                        <?php if (get_field( 'link_orcid' )): ?>
                            <li class="mb-3"><a class="p-3" target="_blank" href="<?php the_field( 'link_orcid' ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/orcid.png" width=75></a></li>
                        <?php endif; ?>
                    </ul>

                    <!-- <a href="<?php the_permalink(); ?>">Leia mais</a> -->
                </div>

            </div>

        </div>

    <?php endwhile; endif; ?>
    
    <?php the_posts_pagination(array(
        'mid_size' => 2,
        'prev_text' => '<span class="prev-page">Anterior</span>',
        'next_text' => '<span class="next-page">Próximo</span>',
        'screen_reader_text' => ' ',
        'before_page_number' => '',
    )); ?>

</div>

<?php get_footer(); ?>